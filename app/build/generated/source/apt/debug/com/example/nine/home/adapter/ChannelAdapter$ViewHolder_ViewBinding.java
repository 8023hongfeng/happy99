// Generated code from Butter Knife. Do not modify!
package com.example.nine.home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.nine.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChannelAdapter$ViewHolder_ViewBinding implements Unbinder {
  private ChannelAdapter.ViewHolder target;

  @UiThread
  public ChannelAdapter$ViewHolder_ViewBinding(ChannelAdapter.ViewHolder target, View source) {
    this.target = target;

    target.ivChannel = Utils.findRequiredViewAsType(source, R.id.iv_channel, "field 'ivChannel'", ImageView.class);
    target.tvChannel = Utils.findRequiredViewAsType(source, R.id.tv_channel, "field 'tvChannel'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ChannelAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivChannel = null;
    target.tvChannel = null;
  }
}
