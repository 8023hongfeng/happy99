package com.example.nine.activity.fragment;

import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.nine.base.BaseFragment;


public class RecordFragment extends BaseFragment {
    private TextView textView;
    @Override
    public View initView() {
        textView = new TextView(mContext);
        textView.setTextSize(40);
        textView.setGravity(Gravity.CENTER);

        return textView;
    }

    @Override
    public void initData() {
        super.initData();
        textView.setText("这是活动页面");
    }
}
