package com.example.nine.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.nine.R;
import com.example.nine.view.MyGridView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HomeBeanAdapter extends RecyclerView.Adapter {
    //  private final HomeBean.ResultBean beanResult;
    /**
     * 上下文
     */
    private Context mContext;

    /**
     * 五种类型
     */
    /**
     * 横幅广告
     */
    public static final int BANNER = 0;
    /**
     * 频道
     */
    public static final int CHANNEL = 1;

    /**
     * 活动
     */
    public static final int NEARBY = 2;

    /**
     * 秒杀
     */
    public static final int SECKILL = 3;
    /**
     * 推荐
     */
    public static final int RECOMMEND = 4;
    /**
     * 热卖
     */
    public static final int HOT = 5;

    /**
     * 当前类型
     */
    public int currentType = BANNER;
    private final LayoutInflater mLayoutInflater;
    private String[] images = {"http://192.168.1.132:8080/a.jpg", "http://192.168.1.132:8080/b.jpg", "http://192.168.1.132:8080/c.jpg"};
    //  private SeckillRecycleViewAdapter seckillRecycleViewAdapter;

    //    public HomeBeanAdapter(Context mContext, HomeBean.ResultBean beanResult) {
//        this.mContext =mContext;
//        this.beanResult = beanResult;
//        mLayoutInflater = LayoutInflater.from(mContext);
//    }
    public HomeBeanAdapter(Context mContext) {
        this.mContext = mContext;
//        this.beanResult = beanResult;
        mLayoutInflater = LayoutInflater.from(mContext);
    }


    /**
     * 创建viewholder，相当于geiview
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override

    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == BANNER) {
            return new MyBannerViewHolder(mLayoutInflater.inflate(R.layout.banner_viewpager, null), mContext);

        } else if (viewType == CHANNEL) {
            return new MyChannelViewHolder(mLayoutInflater.inflate(R.layout.channel_item, null), mContext);
        }
//        else if (viewType == ACT){
//            return new MyActViewHolder(mLayoutInflater.inflate(R.layout.act_item,null),mContext);
//        }else if (viewType == SECKILL){
//            return new MySeckillViewHolder(mLayoutInflater.inflate(R.layout.seckill_item,null),mContext);
//        }else if (viewType == RECOMMEND){
//            return new MyRecommendViewHolder(mLayoutInflater.inflate(R.layout.recommend_item,null),mContext);
//        }
        else if (viewType == NEARBY) {
            return new MyNearbyViewHolder(mLayoutInflater.inflate(R.layout.nearby_item, null), mContext);
        }
        return null;


    }

    private MyNearbyAdapter myNearbyAdapter;
    class MyNearbyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_more_nearby;
        private MyGridView gv_nearby;
        private Context mContext;

        public MyNearbyViewHolder(View view, Context mContext) {
            super(view);
            this.mContext = mContext;
            tv_more_nearby = view.findViewById(R.id.tv_more_nearby);
            gv_nearby = view.findViewById(R.id.gv_nearby);

            gv_nearby.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Toast.makeText(mContext, "position" + position, Toast.LENGTH_SHORT).show();
                }
            });

            myNearbyAdapter = new MyNearbyAdapter(mContext);
            gv_nearby.setAdapter(myNearbyAdapter);
        }

//        public void setData(List<HomeBean.ResultBean.HotInfoBean> hot_info) {
//            myHotAdapter = new MyNearbyAdapter(mContext,hot_info);
//            gv_hot.setAdapter(myHotAdapter);
//        }

    }

//
    class MyChannelViewHolder extends RecyclerView.ViewHolder {
        private Context mContext;

        private GridView gv_channel;

        public MyChannelViewHolder(View view, Context mContext) {
            super(view);
            this.mContext = mContext;
            gv_channel = view.findViewById(R.id.gv_channel);
            gv_channel.setAdapter((new ChannelAdapter(mContext)));
        }

//        public void setData(List<HomeBean.ResultBean.ChannelInfoBean> channel_info) {
//
//            gv_channel.setAdapter((new ChannelAdapter(mContext, channel_info)));
//
//            gv_channel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                    if (position <= 8) {
////                        Intent intent = new Intent(mContext, GoodsListActivity.class);
////                        intent.putExtra("position", position);
////                        mContext.startActivity(intent);
////                    } else {
////
////                    }
//                    Toast.makeText(mContext, "position"+position, Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
    }

    class MyBannerViewHolder extends RecyclerView.ViewHolder {
        public Banner banner;
        public Context mContext;

        public MyBannerViewHolder(View view, Context mContext) {
            super(view);
            banner = (Banner) itemView.findViewById(R.id.banner);
            this.mContext = mContext;

        }

        public void setData(String[] images) {
            List<String> imageUris = new ArrayList<>();
            for (int i = 0; i < images.length; i++) {
                imageUris.add(images[i]);
            }
            banner.setImageLoader(new GlideImageLoader());
            //设置图片集合
            banner.setImages(imageUris);
            //设置banner动画效果
            banner.setBannerAnimation(Transformer.DepthPage);
            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
            //设置自动轮播，默认为true
            banner.isAutoPlay(true);
            //设置轮播时间
            banner.setDelayTime(3500);
            //设置指示器位置（当banner模式中有指示器时）
            banner.setIndicatorGravity(BannerConfig.CENTER);
            //banner设置方法全部调用完毕时最后调用
            banner.start();
            banner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    Toast.makeText(mContext, "position位置为" + position, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public class GlideImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            /**
             注意：
             1.图片加载器由自己选择，这里不限制，只是提供几种使用方法
             2.返回的图片路径为Object类型，由于不能确定你到底使用的那种图片加载器，
             传输的到的是什么格式，那么这种就使用Object接收和返回，你只需要强转成你传输的类型就行，
             切记不要胡乱强转！
             */


            //Glide 加载图片简单用法
            System.out.println("图片地址为" + path);
            Glide.with(context).load(path).into(imageView);

            //Picasso 加载图片简单用法
            //    Picasso.with(context).load(path).into(imageView);

            //用fresco加载图片简单用法，记得要写下面的createImageView方法
//            Uri uri = Uri.parse((String) path);
//            imageView.setImageURI(uri);
        }

        //提供createImageView 方法，如果不用可以不重写这个方法，主要是方便自定义ImageView的创建
//        @Override
//        public ImageView createImageView(Context context) {
//            //使用fresco，需要创建它提供的ImageView，当然你也可以用自己自定义的具有图片加载功能的ImageView
//            SimpleDraweeView simpleDraweeView=new SimpleDraweeView(context);
//            return simpleDraweeView;
//        }
    }

    /**
     * 绑定数据
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == BANNER) {
            MyBannerViewHolder bannerViewHolder = (MyBannerViewHolder) holder;
            bannerViewHolder.setData(images);
        }
        //    bannerViewHolder.setData(beanResult.getBanner_info());
//            }
        else if (getItemViewType(position) == CHANNEL) {
            MyChannelViewHolder channelViewHolder = (MyChannelViewHolder) holder;
            //  channelViewHolder.setData(beanResult.getChannel_info());
        }
//            else if(getItemViewType(position)==ACT){
//                MyActViewHolder actViewHolder = (MyActViewHolder) holder;
//                actViewHolder.setData(beanResult.getAct_info());
//            }else if(getItemViewType(position)==SECKILL){
//                MySeckillViewHolder seckillViewHolder= (MySeckillViewHolder) holder;
//                seckillViewHolder.setData(beanResult.getSeckill_info());
//            }else if(getItemViewType(position)==RECOMMEND){
//                MyRecommendViewHolder recommendViewHolder= (MyRecommendViewHolder) holder;
//                recommendViewHolder.setData(beanResult.getRecommend_info());
//            }
        else if (getItemViewType(position) == NEARBY) {
            MyNearbyViewHolder nearbyViewHolder = (MyNearbyViewHolder) holder;
            // nearbyViewHolder.setData(beanResult.getHot_info());
        }

    }

    /**
     * 返回数据类型
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {

        switch (position) {
            case BANNER:
                currentType = BANNER;
                break;
            case CHANNEL:
                currentType = CHANNEL;
                break;
            case NEARBY:
                currentType = NEARBY;
                break;
            case SECKILL:
                currentType = SECKILL;
                break;
            case RECOMMEND:
                currentType = RECOMMEND;
                break;
            case HOT:
                currentType = HOT;
                break;
        }
        return currentType;
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
