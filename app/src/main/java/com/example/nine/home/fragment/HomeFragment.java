package com.example.nine.home.fragment;

import android.view.View;
import android.widget.TextView;

import com.example.nine.R;
import com.example.nine.base.BaseFragment;
import com.example.nine.base.BasePager;
import com.example.nine.home.adapter.HomeBeanAdapter;

import java.util.ArrayList;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {


    @BindView(R.id.home_recycle)
    RecyclerView homeRecycle;
    private ArrayList list;
    private ArrayList<BasePager> basePagers;

    private TextView textView;

    private HomeBeanAdapter homeBeanAdapter;
    @Override
    public View initView() {
        View view = View.inflate(mContext, R.layout.home_fragment, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void initData() {
        super.initData();

        homeBeanAdapter = new HomeBeanAdapter(mContext);
        homeRecycle.setAdapter(homeBeanAdapter);
        GridLayoutManager manager = new GridLayoutManager(mContext, 1);
        homeRecycle.setLayoutManager(manager);
    }


}
