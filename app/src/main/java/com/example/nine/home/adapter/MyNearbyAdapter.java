package com.example.nine.home.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nine.R;

public class MyNearbyAdapter extends BaseAdapter {
    private final Context mContext;
    private String[] name ={"签到","广场舞","活动","附近"};
    private String[] name2 ={"2500","200","40","4000"};
    private int[] images_user ={R.drawable.home_sign,R.drawable.home_square,R.drawable.home_activity,R.drawable.home_nearby};
    private int[] images ={R.drawable.aa,R.drawable.bb,R.drawable.cc,R.drawable.dd};

    // private final List<HomeBean.ResultBean.HotInfoBean> hot_info;

//    public MyNearbyAdapter(Context mContext, List<HomeBean.ResultBean.HotInfoBean> hot_info) {
//        this.mContext = mContext;
//        this.hot_info = hot_info;
//    }


    public MyNearbyAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
//        return hot_info.size();
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view ==null){
            view = View.inflate(mContext, R.layout.item_nearby_grid_view,null);
            viewHolder = new ViewHolder();
            viewHolder.iv_nearby = view.findViewById(R.id.iv_nearby);
            viewHolder.round_head = view.findViewById(R.id.round_head);
            viewHolder.tv_name = view.findViewById(R.id.tv_name);
            viewHolder.tv_total = view.findViewById(R.id.tv_total);

            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

//        Glide.with(mContext)
//                .load(Contast.base_image_url + hot_info.get(i).getFigure())
//                .into(viewHolder.iv_hot);
//        viewHolder.tv_name.setText(hot_info.get(i).getName());
//        viewHolder.tv_price.setText(hot_info.get(i).getCover_price());
        Glide.with(mContext)
                .load(images_user[i])

                .into(viewHolder.round_head);
        Glide.with(mContext)
                .load(images[i])
                .into(viewHolder.iv_nearby);
        viewHolder.tv_name.setText(name[i]);
        viewHolder.tv_total.setText(name2[i]);
        return view;
    }

    static class ViewHolder{
        ImageView iv_nearby;
        ImageView round_head;
        TextView tv_name;
        TextView  tv_total;
    }
}
