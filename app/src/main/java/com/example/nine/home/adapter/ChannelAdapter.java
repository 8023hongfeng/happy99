package com.example.nine.home.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.nine.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelAdapter extends BaseAdapter {

    private Context mContext;

    private String[] drec ={"签到","广场舞","活动","附近"};
    private int[] images ={R.drawable.home_sign,R.drawable.home_square,R.drawable.home_activity,R.drawable.home_nearby};
  //  private List<HomeBean.ResultBean.ChannelInfoBean> channel_info;
    public ChannelAdapter(Context mContext) {
        this.mContext = mContext;
//        this.channel_info = channel_info;
    }
// public ChannelAdapter(Context mContext, List<HomeBean.ResultBean.ChannelInfoBean> channel_info) {
//        this.mContext = mContext;
////        this.channel_info = channel_info;
//    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view ==null){
            view = View.inflate(mContext, R.layout.item_channel,null);
            viewHolder = new ViewHolder(view);
            viewHolder.ivChannel = view.findViewById(R.id.iv_channel);
            viewHolder.tvChannel = view.findViewById(R.id.tv_channel);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }

     //   HomeBean.ResultBean.ChannelInfoBean channelInfoBean = channel_info.get(i);
      //  viewHolder.tvChannel.setText(channelInfoBean.getChannel_name());
        viewHolder.tvChannel.setText(drec[i]);
        Glide.with(mContext)
//                .load(Contast.base_image_url +channelInfoBean.getImage())
                .load(images[i])
                .into(viewHolder.ivChannel);

        return view;
    }

    class ViewHolder {
        @BindView(R.id.iv_channel)
        ImageView ivChannel;
        @BindView(R.id.tv_channel)
        TextView tvChannel;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
